using System;

namespace GameFramework.FairyGUI.Runtime
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class FairyGUIComponentAttribute : Attribute
    {
        public string URL { get; }
        
        public FairyGUIComponentAttribute(string url)
        {
            this.URL = url;
        }
    }
}